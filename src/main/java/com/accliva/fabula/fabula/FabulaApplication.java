package com.accliva.fabula.fabula;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FabulaApplication {

	public static void main(String[] args) {
		SpringApplication.run(FabulaApplication.class, args);
	}

}
