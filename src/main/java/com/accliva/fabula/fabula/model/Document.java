/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accliva.fabula.fabula.model;

import com.accliva.fabula.fabula.model.authorization.IResource;
import com.accliva.fabula.fabula.model.domain.Domain;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;

/**
 *
 * @author caio
 */
@Entity
public class Document implements IResource {

    @Id
    @GeneratedValue
    private UUID id;

    private String title;
    private String subtitle;
    private String contents;
    @ManyToOne
    @MapsId("domain")
    @JoinColumn(name = "domain")
    Domain domain;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime creationDateTime;

//    @OneToMany(mappedBy = "document", cascade = CascadeType.ALL)
//    Set<Authorship> autorships;

    boolean restricted;

    public Document(String title, String subtitle, String contents, boolean isPrivate) {
        this.title = title;
        this.subtitle = subtitle;
        this.contents = contents;
        this.creationDateTime = LocalDateTime.now();
        this.restricted = isPrivate;
    }

    public Document() {

    }

    public boolean isRestricted() {
        return restricted;
    }

    public void setRestricted(boolean restricted) {
        this.restricted = restricted;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * @param subtitle the subtitle to set
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * @return the contents
     */
    public String getContents() {
        return contents;
    }

    /**
     * @param contents the contents to set
     */
    public void setContents(String contents) {
        this.contents = contents;
    }

    public LocalDateTime getCreationDateTime() {
        return this.creationDateTime;
    }
    
    public Domain getDomain(){
        return this.domain;
    }
}
