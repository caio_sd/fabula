/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accliva.fabula.fabula.model.accounts;

import com.accliva.fabula.fabula.model.authorization.IResource;
import java.time.LocalDateTime;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author caio
 */
@Entity
public class User implements IResource {

    @Id
    private String username;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime creationDateTime;

//    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
//    Set<Authorship> autorships;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    Set<Account> accounts;

    public User() {
    }

    public User(String username) {
        this.username = username;
    }

    public LocalDateTime getCreationDateTime() {
        return this.creationDateTime;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return "senha";
    }

}
