/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accliva.fabula.fabula.model.authorization;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.http.HttpMethod;

/**
 *
 * @author caio
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"resource", "effect", "action"}))
public class Permission implements IResource {

    @Id
    @GeneratedValue
    Long id;

    Class<? extends IResource> resource;
    Boolean effect;
    HttpMethod action;

    @ManyToMany(mappedBy = "permissions")
    @OnDelete(action = OnDeleteAction.CASCADE)
    Set<Role> roles;

    public Permission() {
    }

    public Permission(Class<? extends IResource> resource, Boolean effect, HttpMethod action) {
        this.resource = resource;
        this.action = action;
        this.effect = effect;
    }

    public Long getId() {
        return id;
    }

    public Set<Role> getRoles() {
        return Set.copyOf(roles);
    }

    public Class<? extends IResource> getResource() {
        return resource;
    }

    public Boolean getEffect() {
        return effect;
    }

    public HttpMethod getAction() {
        return action;
    }
}
