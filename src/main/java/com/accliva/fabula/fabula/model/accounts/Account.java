/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accliva.fabula.fabula.model.accounts;

import com.accliva.fabula.fabula.model.authorization.IResource;
import com.accliva.fabula.fabula.model.authorization.Role;
import com.accliva.fabula.fabula.model.domain.Domain;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author caio
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"username", "domain"}))
public class Account implements IResource {

    @EmbeddedId
    AccountId id;

    @ManyToOne
    @MapsId("username")
    @JoinColumn(name = "username")
    @NotNull
    User user;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime creationDateTime;

    @ManyToOne//(cascade = CascadeType.ALL)
    @MapsId("domain")
    @JoinColumn(name = "domain")
    Domain domain;

    @ManyToMany(mappedBy = "accounts", cascade = CascadeType.ALL)
    Set<Role> roles;

    public Account() {
    }

    public Account(User user, Domain domain) {
        this.user = user;
        this.domain = domain;
        this.id = new AccountId(domain.getId(), user.getUsername());
        this.creationDateTime = LocalDateTime.now();
    }

    public AccountId getId(){
        return this.id;
    }
    
    public User getUser() {
        return user;
    }

    public Domain getDomain() {
        return this.domain;
    }
    
    public void setDomain(Domain domain){
        this.domain = domain;
    }

    public boolean hasDomain() {
        return this.domain != null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Account other = (Account) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
