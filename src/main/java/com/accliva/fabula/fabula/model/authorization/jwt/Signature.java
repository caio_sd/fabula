/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accliva.fabula.fabula.model.authorization.jwt;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author caio
 */
public class Signature extends HashMap<String, Object> implements JwtComponent {

    private final Jwt jwt;
    private final String encoded;

    public Signature(Jwt jwt, String secretKey) throws InvalidKeyException, IOException {
        this.jwt = jwt;
        this.encoded = sign(secretKey);
    }
    
    public Signature(String encoded, Jwt jwt){
        this.jwt = jwt;
        this.encoded = encoded;
    }

    static  byte[] calcHmacSha256(byte[] secretKey, byte[] message) throws InvalidKeyException {
        byte[] hmacSha256 = null;
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, "HmacSHA256");
            mac.init(secretKeySpec);
            hmacSha256 = mac.doFinal(message);
        } catch (InvalidKeyException e) {
            throw new InvalidKeyException();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException();
        }
        return hmacSha256;
    }

    private String sign(String secretKey) throws InvalidKeyException, IOException {

        String data = jwt.getHeader().encode() + "." + jwt.getPayload().encode();

        byte[] base64 = calcHmacSha256(secretKey.getBytes(StandardCharsets.UTF_8), data.getBytes(StandardCharsets.UTF_8));
        return Base64.getUrlEncoder().withoutPadding().encodeToString(base64);
    }

    @Override
    public String encode() {
        return encoded;
    }

    public Jwt getJwt() {
        return jwt;
    }

    public String getEncoded() {
        return encoded;
    }

    
    public static void main(String[] args) throws InvalidJwtException, InvalidKeyException {
        String bearer = "eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9."+
                "eyJzdWIiOiJyb290QGRvbWFpbi5jb20iLCJkb21haW5zIjp7ImRkYWYyYTczLWQ3MjAtNDNlNC05NmRmLTUzMzk3ZmU2ZGM3MyI6IkZhYnVsYSBQcm9qZWN0IiwiNzVlZDFiNDItZGU0NC00NzgzLThjYTQtZDE0MWNlNTJhMzY3IjoiRmFidWxhIiwiOGEwNzFlNzEtNzk1Zi00ZWFjLWFjNGEtY2MzMzRmYmU1ZDQ3IjoiRmFidWxhIEJsb2cifSwiZXhwIjoxNjQ5MDEzMDIwMDk2LCJpYXQiOjE2NDg5OTUwMjAwOTZ9."+
                "zU1ip-_TXnlkKN0MNxPug-9pWtOzhw5oDfcoxGmsZCg";
        String[] parts = bearer.split("\\.");
        Jwt jwt = new Jwt(bearer);
    
        
        System.err.println("secret base64 = "+ Base64.getUrlEncoder().withoutPadding().encodeToString("secretsecret".getBytes()) );
        System.err.println(jwt.getHeader());
        System.err.println(jwt.getPayload());
        System.err.println(jwt.getSignature());
        
//        String data = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9";
        String data = parts[0]+"."+parts[1];
        byte[] base64 = calcHmacSha256("secret".getBytes(), data.getBytes(StandardCharsets.UTF_8));
        System.err.println(Base64.getUrlEncoder().withoutPadding().encodeToString(base64));
    }
}
