/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accliva.fabula.fabula.model.domain;

import com.accliva.fabula.fabula.model.Document;
import com.accliva.fabula.fabula.model.accounts.Account;
import com.accliva.fabula.fabula.model.authorization.IResource;
import com.accliva.fabula.fabula.model.authorization.Role;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author caio
 */
@Entity
public class Domain implements IResource {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(unique = true)
    private String name;

    @OneToMany(mappedBy = "domain", cascade = CascadeType.ALL)
    Set<Account> accounts;

    @OneToMany(mappedBy = "domain")//, cascade = CascadeType.ALL)
    Set<Document> documents;

    @ManyToOne
    Domain parent;
    
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    Set<Domain> children;

    @OneToMany(mappedBy = "domain", cascade = CascadeType.ALL)//, cascade = CascadeType.ALL)
    Set<Role> roles;

    private boolean restricted;

    public Domain() {
    }

    public Domain(String name) {
        this.name = name;
    }

    public Domain(String name, Domain parent) {
        this.name = name;
        this.parent = parent;
    }

//    public Domain getParent() {
//        return parent;
//    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Domain other = (Domain) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public UUID getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public boolean isRestricted() {
        return restricted;
    }

    public void setRestricted(boolean restricted) {
        this.restricted = restricted;
    }

//    public Set<Account> getAccounts() {
//        return accounts;
//    }
//
//    public void setAccounts(Set<Account> accounts) {
//        this.accounts = accounts;
//    }
}
