/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accliva.fabula.fabula.repository;

import com.accliva.fabula.fabula.model.Document;
import java.util.List;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author caio
 */
@Transactional 
public interface DocumentRepository extends CrudRepository<Document, UUID> {

    List<Document> findByTitle(String title);
//    List<Document> findByAuthor(String authorName);
}
