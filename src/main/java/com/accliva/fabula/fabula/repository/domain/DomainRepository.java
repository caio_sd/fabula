/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accliva.fabula.fabula.repository.domain;

import com.accliva.fabula.fabula.model.domain.Domain;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import java.util.Optional;
import java.util.Set;
import javax.transaction.Transactional;

/**
 *
 * @author caio
 */
@Transactional 
public interface DomainRepository extends CrudRepository<Domain, UUID> {

    Optional<Domain> findByName(String name);
    Set<Domain> findByParent(Domain parent);
    Set<Domain> findByRestrictedFalse();
}
