/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accliva.fabula.fabula.repository.accounts;

import com.accliva.fabula.fabula.model.accounts.User;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author caio
 */
@Transactional 
public interface UserRepository extends CrudRepository<User, String> {
    
}
