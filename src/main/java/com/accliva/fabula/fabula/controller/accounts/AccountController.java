/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accliva.fabula.fabula.controller.accounts;

import com.accliva.fabula.fabula.model.accounts.Account;
import com.accliva.fabula.fabula.model.domain.Domain;
import com.accliva.fabula.fabula.model.accounts.User;
import com.accliva.fabula.fabula.model.authorization.jwt.InvalidJwtException;
import com.accliva.fabula.fabula.service.accounts.UserAndAccountService;
import com.accliva.fabula.fabula.service.authorization.AuthorizationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author caio
 */
@RestController
@RequestMapping("/api")
public class AccountController {

    @Autowired
    UserAndAccountService accountsService;
    @Autowired
    AuthorizationService authorizationService;

    @Operation(summary = "Get all the accounts available for the user")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "OK", content = @Content),
        @ApiResponse(responseCode = "400", description = "Invalid headers or parameters", content = @Content),
        @ApiResponse(responseCode = "403", description = "The account does not have the necessary permissions", content = @Content),
        @ApiResponse(responseCode = "501", description = "Internal server error", content = @Content)})
    @GetMapping("/accounts")
    public ResponseEntity<Set<Account>> listAccounts(@RequestHeader("Authorization") String bearer) {
        try {
            Optional<Account> optionalAccount = accountsService.decodeAccount(bearer);
            if (optionalAccount.isPresent()) {
                Account account = optionalAccount.get();
                if (account.hasDomain()) {
                    Domain domain = account.getDomain();
                    if (authorizationService.verify(account, domain, Account.class, HttpMethod.GET)) {
                        Set<Account> accounts = accountsService.getAccounts(domain);
                        return new ResponseEntity<>(accounts, HttpStatus.OK);
                    } else {
                        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                    }

                } else {
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } else {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

            }
        } catch (InvalidJwtException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    
 
}
