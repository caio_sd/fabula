/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accliva.fabula.fabula.controller.document;

import com.accliva.fabula.fabula.model.Document;
import com.accliva.fabula.fabula.repository.DocumentRepository;
import com.accliva.fabula.fabula.service.accounts.UserAndAccountService;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author caio
 */
@RestController
@RequestMapping("/api")
public class DocumentController {

    @Autowired
    DocumentRepository documentRepository;
//    @Autowired
//    AuthorshipRepository authorshipRepository;
    @Autowired
    UserAndAccountService accountsService;


    @GetMapping("/documents/{id}")
    public ResponseEntity<Document> getDocumentById(@PathVariable("id") UUID id) {
        Optional<Document> documentData = documentRepository.findById(id);
        if (documentData.isPresent()) {
            return new ResponseEntity<>(documentData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

//    @PostMapping("/documents")
//    public ResponseEntity<Document> createTutorial(@RequestBody Document document) {
//        try {
//            Document newDocument = documentRepository
//                    .save(new Document(document.getTitle(), document.getSubtitle(), document.getContents()));
//            return new ResponseEntity<>(newDocument, HttpStatus.CREATED);
//        } catch (Exception e) {
//            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
}
