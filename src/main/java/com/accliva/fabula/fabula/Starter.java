/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accliva.fabula.fabula;

import com.accliva.fabula.fabula.model.Document;
import com.accliva.fabula.fabula.model.accounts.Account;
import com.accliva.fabula.fabula.model.accounts.User;
import com.accliva.fabula.fabula.model.authorization.Permission;
import com.accliva.fabula.fabula.model.authorization.Role;
import com.accliva.fabula.fabula.model.domain.Domain;
import com.accliva.fabula.fabula.repository.authorization.RoleRepository;
import com.accliva.fabula.fabula.repository.domain.DomainRepository;
import com.accliva.fabula.fabula.service.accounts.UserAndAccountService;
import com.accliva.fabula.fabula.service.accounts.DomainService;
import com.accliva.fabula.fabula.service.authorization.AuthorizationService;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 *
 * @author caio
 */
@Component
public class Starter implements InitializingBean {

    @Autowired
    UserAndAccountService accountService;
    @Autowired
    AuthorizationService authorizationService;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    DomainService domainService;
    @Autowired
    DomainRepository domainRepository;

    @Override
    public void afterPropertiesSet() throws Exception {

        User rootUser = accountService.createUser("root@domain.com").get();
        Domain fabula = domainService.createDomain(rootUser, "Fabula").get();
        Domain fabulaBlog = domainService.createDomain(rootUser, "Fabula Blog", fabula).get();
        Domain fabulaProject = domainService.createDomain(rootUser, "Fabula Project", fabula).get();

        User demoUser1 = accountService.createUser("demo1@domain.com").get();
        User demoUser2 = accountService.createUser("demo2@domain.com").get();
        User demoUser3 = accountService.createUser("demo3@domain.com").get();
//
        Account rootFabulaAccount = accountService.createOrRecoverAccount(rootUser, fabula).get();
        Account demoUser1FabulaAccount = accountService.createOrRecoverAccount(demoUser1, fabula).get();
        Account demoUser2FabulaBlogAccount = accountService.createOrRecoverAccount(demoUser2, fabulaBlog).get();
        Account demoUser2FabulaProjectAccount = accountService.createOrRecoverAccount(demoUser2, fabulaProject).get();
        Account demoUser3FabulaProjectAccount = accountService.createOrRecoverAccount(demoUser3, fabulaProject).get();
//

        HttpMethod[] actions = HttpMethod.values();
        Set<Permission> permissions = new HashSet<>();
        permissions.addAll(authorizationService.createOrRecoverPermissions(true, Document.class, actions));
        permissions.addAll(authorizationService.createOrRecoverPermissions(true, Permission.class, actions));
        permissions.addAll(authorizationService.createOrRecoverPermissions(true, User.class, actions));
        permissions.addAll(authorizationService.createOrRecoverPermissions(true, Account.class, actions));
        permissions.addAll(authorizationService.createOrRecoverPermissions(true, Domain.class, actions));
////        permissions.addAll(authorizationService.createOrRecoverPermissions(true, Authorship.class, actions));
//
        Role fabulaAdmin = authorizationService.createRole("admin", fabula, permissions);
        fabulaAdmin.addAccount(rootFabulaAccount);
        fabulaAdmin = authorizationService.saveRole(fabulaAdmin);
        Role fabulaDocumentAll = authorizationService.createRole("fabulaReadAll", fabula, authorizationService.createOrRecoverPermissions(true, Document.class, actions));
        fabulaDocumentAll.addAccount(demoUser1FabulaAccount);
        fabulaDocumentAll = authorizationService.saveRole(fabulaDocumentAll);
        Role fabulaDomainAll = authorizationService.createRole("fabulaReadAll", fabula, authorizationService.createOrRecoverPermissions(true, Domain.class, actions));
        fabulaDomainAll.addAccount(demoUser1FabulaAccount);
        fabulaDomainAll = authorizationService.saveRole(fabulaDomainAll);
        Role fabulaBlogDocumentAll = authorizationService.createRole("fabulaReadAll", fabulaBlog, authorizationService.createOrRecoverPermissions(true, Domain.class, actions));
        fabulaBlogDocumentAll.addAccount(demoUser2FabulaBlogAccount);
        fabulaBlogDocumentAll = authorizationService.saveRole(fabulaBlogDocumentAll);
        Role fabulaProjectAccountAll = authorizationService.createRole("fabulaReadAll", fabulaProject, authorizationService.createOrRecoverPermissions(true, Account.class, actions));
        fabulaProjectAccountAll.addAccount(demoUser2FabulaProjectAccount);
        fabulaProjectAccountAll.addAccount(demoUser3FabulaProjectAccount);
        fabulaProjectAccountAll = authorizationService.saveRole(fabulaProjectAccountAll);

        domainService.deleteDomain(fabula);  
        accountService.deleteAllAccounts(fabula);  
//        domainService.recursiveDeleteDomain(domainService.getDomain(fabula.getId()).get());
    }

}
